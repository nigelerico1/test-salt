import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:testsalt/cubit/auth/auth_cubit.dart';
import 'package:testsalt/model/dto/user_model.dart';
import 'package:testsalt/services/user_services.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super(UserInitial());

  final UserService _userService = UserService();
  List<UserDataModel> userList= [];


  void getUserList({int? page}) async {
    if (state is GetUserInitial) return;

    emit(GetUserInitial(userList: userList));

    UserModel apiResult = await _userService.getUserList(page: page);

    if(apiResult.data != null){
      userList.addAll(apiResult.data ?? []);
      emit(GetUserSuccess(userList: userList, totalPage: apiResult.totalPages ?? 0, page: apiResult.page ?? 0));
    } else{
      emit(GetUserFailed());
    }
  }
}
