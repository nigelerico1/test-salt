part of 'user_cubit.dart';

abstract class UserState extends Equatable {
  const UserState();

  @override
  List<Object> get props => [];
}

class UserInitial extends UserState {}

class GetUserInitial extends UserState {
  final List<UserDataModel> userList;

  GetUserInitial({required this.userList});
}

class GetUserSuccess extends UserState {
  final List<UserDataModel> userList;
  final int totalPage;
  final int page;

  const GetUserSuccess( {required this.userList, required this.totalPage, required this.page });
}

class GetUserFailed extends UserState {}
