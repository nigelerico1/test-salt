part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class LoginInitial extends AuthState {}

class LoginSuccess extends AuthState {
  final String token;

  const LoginSuccess({required this.token});
}

class LoginError extends AuthState {
  final String error;

  const LoginError({required this.error});

}

