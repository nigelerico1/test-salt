import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:testsalt/app.dart';
import 'package:testsalt/model/dto/login_model.dart';
import 'package:testsalt/model/request/login_request.dart';
import 'package:testsalt/services/login_services.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  final LoginService _loginService = LoginService();

  void login(LoginRequest data) async {
    emit(LoginInitial());

    LoginModel apiResult = await _loginService.login(data: data);

    if(apiResult.token != null){
      App().preferences!.setString("token", apiResult.token!);

      emit(LoginSuccess(token: apiResult.token ?? ""));
    } else{

      emit(LoginError(error: apiResult.error ?? ""));
    }
  }
}
