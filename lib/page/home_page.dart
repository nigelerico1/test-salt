import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:testsalt/app.dart';
import 'package:testsalt/cubit/auth/auth_cubit.dart';
import 'package:testsalt/cubit/user/user_cubit.dart';
import 'package:testsalt/model/dto/user_model.dart';
import 'package:testsalt/page/login_page.dart';
import 'package:testsalt/widget/list_view_people.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  late UserCubit userCubit;

  @override
  void initState() {
    userCubit = BlocProvider.of<UserCubit>(context);
    userCubit.getUserList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
          actions: [
            IconButton(
              icon: const Icon(Icons.logout),
              tooltip: 'logout',
              onPressed: () {
                App().preferences!.remove('token');
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const LoginPage()));
              },
            ),
          ],
        ),
        body: BlocBuilder<UserCubit, UserState>(
          builder: (context, state) {
            List<UserDataModel> oldList  = [];

            if(state is GetUserInitial) {
                oldList = state.userList;
            }

            if(state is GetUserInitial && state.userList.isEmpty) {
              oldList = state.userList;
              return const Center(
                child: CircularProgressIndicator(),
              );

            } else if (state is GetUserSuccess ){
              return LazyLoadScrollView(
                onEndOfPage: (){
                  if(state.totalPage > state.page) {
                    userCubit.getUserList(page: state.page + 1);
                  }
                },
                child: ListViewPeople(list: state.userList),
              );
            } else {
              return ListViewPeople(list: oldList);
            }
          },
        )
    );
  }
}


