import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testsalt/cubit/auth/auth_cubit.dart';
import 'package:testsalt/page/home_page.dart';
import 'package:testsalt/model/request/login_request.dart';
import 'package:testsalt/widget/LoadingDialog.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  late AuthCubit authCubit;

  @override
  void initState() {
    authCubit = BlocProvider.of<AuthCubit>(context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              SaltTextField(
                emailController: _emailController,
                textInputType: TextInputType.emailAddress,
                label: "Enter your email",
              ),
              const SizedBox(
                height: 16,
              ),
              SaltTextField(
                emailController: _passwordController,
                textInputType: TextInputType.visiblePassword,
                obscureText: true,
                label: "Enter your password",
              ),
              const SizedBox(
                height: 16,
              ),
              BlocConsumer<AuthCubit, AuthState>(
                listener: (context, state) {
                  if (state is LoginInitial) {
                    LoadingDialog(title: 'Loading', description: 'Please Wait..').show(context);
                  } else if (state is LoginSuccess) {
                    Navigator.pop(context);
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const HomePage()));
                  } else if (state is LoginError) {
                    Navigator.pop(context);
                    final snackBar = SnackBar(
                      content: Text(state.error ?? ""),
                    );

                    print(state.error);

                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
                builder: (context, state) {
                  return OutlinedButton(
                    onPressed: () {
                      authCubit.login(LoginRequest(
                          email: _emailController.text,
                          password: _passwordController.text));
                    },
                    child: const Text('Login'),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SaltTextField extends StatelessWidget {
  const SaltTextField({
    super.key,
    required TextEditingController emailController,
    required this.textInputType,
    required this.label,
    this.obscureText = false,
  }) : _emailController = emailController;

  final TextEditingController _emailController;
  final TextInputType textInputType;
  final String label;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _emailController,
      keyboardType: textInputType,
      obscureText: obscureText,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        labelText: label,
      ),
    );
  }
}
