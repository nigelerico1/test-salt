import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:testsalt/model/dto/user_model.dart';

class ListViewPeople extends StatelessWidget {
  const ListViewPeople({
    super.key, required this.list,
  });

  final List<UserDataModel> list;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        itemCount: list.length,
        itemBuilder: (context, int index) {
          UserDataModel data =  list[index];
          return Card(
            child: ListTile(
              leading: CachedNetworkImage(
                imageUrl: data.avatar ?? "",
                placeholder: (context, url) => const CircularProgressIndicator(),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
              title: Text("${data.firstName} ${data.lastName}"),
              subtitle: Text(data.email ?? ""),
              isThreeLine: true,
            ),
          );
        });
  }
}