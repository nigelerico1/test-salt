import 'package:dio/dio.dart';
import 'package:testsalt/app.dart';
import 'package:testsalt/model/dto/login_model.dart';
import 'package:testsalt/model/request/login_request.dart';

class LoginService {
  final dio = App().dio;

  Future<LoginModel> login({required LoginRequest data}) async {
    try {
      Response response = await dio.post("api/login", data: data);

      return LoginModel(
        token: response.data['token']
      );
    } on DioException catch (e) {
      return LoginModel(error: e.response?.data["error"]);
    }
  }

}
