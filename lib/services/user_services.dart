import 'package:dio/dio.dart';
import 'package:testsalt/app.dart';
import 'package:testsalt/model/dto/login_model.dart';
import 'package:testsalt/model/dto/user_model.dart';
import 'package:testsalt/model/request/login_request.dart';

class UserService {
  final dio = App().dio;

  Future<UserModel> getUserList({int? page}) async {
    try {
      Response response = await dio.get("api/users", queryParameters: {
        'page' : page,
        'per_page' : "5"
      });

      return UserModel.fromJson(response.data);
    } on DioException catch (e) {
      throw Exception(e.message);
    }
  }

}
