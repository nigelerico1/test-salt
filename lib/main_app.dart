import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testsalt/app.dart';
import 'package:testsalt/cubit/auth/auth_cubit.dart';
import 'package:testsalt/cubit/user/user_cubit.dart';
import 'package:testsalt/page/home_page.dart';
import 'package:testsalt/page/login_page.dart';


class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    AuthCubit authCubit = AuthCubit();
    UserCubit userCubit = UserCubit();

    bool? isLoggedIn = App().preferences!.getString('token') != null;

    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context)=> authCubit),
        BlocProvider(create: (context)=> userCubit),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Test Salt",
        theme: ThemeData(
          useMaterial3: true,
        ),
        home: isLoggedIn ? const HomePage() : const LoginPage(),
      ),
    );
  }
}
