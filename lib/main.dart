import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testsalt/app.dart';
import 'package:testsalt/cubit/bloc_observer.dart';
import 'package:testsalt/main_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  App.configure();
  Bloc.observer = MyBlocObserver();



  await App().init();


  runApp(const MainApp());
}




