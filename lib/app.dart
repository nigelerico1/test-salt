import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class App {
  static App? _instance;
  late Dio dio;
  SharedPreferences? preferences;

  App.configure() {
    _instance = this;
  }

  factory App() {
    if(_instance == null){
      throw UnimplementedError("App harus diinialisasi");
    }

    return _instance!;
  }

  Future<void> init() async {
    preferences = await SharedPreferences.getInstance();

    dio = Dio(BaseOptions(
        baseUrl: "https://reqres.in/",
        connectTimeout: const Duration(seconds: 60),
        receiveTimeout: const Duration(seconds: 60),
        contentType: Headers.jsonContentType,
        responseType: ResponseType.json));

    dio.options.headers = {
      'Authorization': "Bearer ${preferences!.get("token")}"
    };
  }
}
